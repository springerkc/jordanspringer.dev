const express = require('express');
const path = require('path');
// const bodyParser = require('body-parser');

const app = express();
const port = process.env.PORT || 3000;

app.use(express.static(path.join(__dirname, '/public/')));
app.set('views', './src/views');
app.set('view engine', 'ejs');

const nav = [
  { link: '/', title: 'home' },
  { link: '/resume', title: 'resume' },
];

app.get('/', (req, res) => {
  res.render('index', {
    nav,
    pageTitle: 'jordan springer | full stack web developer',
  });
});

app.listen(port, () => {
  console.log(`app running on port ${port}`);
});
